#include <iostream>
#include "stack/Stack.h"
#include "stack/Stack.cpp"
#include <fstream>

using namespace std;

bool checkLine(string line, int lineNumber);

struct CloseOpenSymbol {
    char open;
    char close;
};
CloseOpenSymbol openCloseSymbols[] = {{'{', '}'},
                                      {'(', ')'},
                                      {'[', ']'}};

Stack<char> symbolStack;
int lineCount = 0;

int main() {
    cout << "Input file path to read \n";
    string filePath;
    cin >> filePath;

    ifstream file;
    file.open(filePath);

    string line;
    bool isOk = true;
    int lineNumber = 0;
    while ( getline (file,line) ) {
        if(!checkLine(line, lineNumber)) {
            isOk = false;
            cout << "There is an error on line " << lineCount + 1;
            break;
        }
        lineNumber ++;
    }
    file.close();
    if(isOk && symbolStack.isEmpty()) {
        cout << "file is correct";
    } else if(!symbolStack.isEmpty()) {
        cout << "Unexpected end of file";
    }
    return 0;
}

bool checkLine(string line, int lineNumber) {
    for(char& c : line) {
        for(CloseOpenSymbol& symbol : openCloseSymbols) {
            if(symbol.open == c) {
                symbolStack.push(c);
                lineCount = lineNumber;
            }

            if(symbol.close == c) {
                if(symbolStack.isEmpty())
                    return false;
                char lastOpenedSymbol = symbolStack.pop();
                if(lastOpenedSymbol != symbol.open)
                    return false;
            }
        }
    }
    return true;
}