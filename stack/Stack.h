//
// Created by oem on 20.09.2019.
//

#ifndef UNTITLED_STACK_H
#define UNTITLED_STACK_H


template<typename T>
struct StackItem {
    StackItem* previous;
    T value;
};

template<typename T>
class Stack {
private:
    long size;
    StackItem<T>* first;

public:
    Stack();
    ~Stack() = default;
    Stack& operator =(const Stack &) = default;
    /*
     * Inserts typed element
     * into the stack
     * */
    void push(T item);

    /*
     * Returns the last element of the stack
     * and deletes this element
     * */
    T pop();
    /*
     * Just returns the last element of the stack
     * */
    T get();
    /*
     * Returns the stack size
     * */
    long getSize();

    /*
     * Clear all stack
     * */
    void clear();

    /*
    * Checks if Stack is empty
    * */
    bool isEmpty();

};


#endif //UNTITLED_STACK_H
