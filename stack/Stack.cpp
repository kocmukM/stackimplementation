//
// Created by oem on 20.09.2019.
//

#include "Stack.h"

template<typename T>
Stack<T>::Stack() {
    size = 0;
}

template<typename T>
void Stack<T>::push(T item) {
    auto* stackItem = new StackItem<T>{first, item};
    first = stackItem;
    size++;
}

template<typename T>
T Stack<T>::get() {
    return first->value;
}

template<typename T>
T Stack<T>::pop() {
    T value = first->value;
    first = first->previous;
    size--;
    return value;
}

template<typename T>
long Stack<T>::getSize() {
    return size;
}

template<typename T>
void Stack<T>::clear() {
    first = 0;
}

template<typename T>
bool Stack<T>::isEmpty() {
    return size == 0;
}
